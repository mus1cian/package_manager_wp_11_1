const sumar = require('../index');
const assert = require('assert');

// Assert = afirmación
// 50% test

describe("Probar la suma de 2 números", () => {
    //afirmamos que 5+5 = 10
    it("5+5 = 10", () => {
        assert.equal(10, sumar(5, 5));
    })

    it("5+7 != 10", () => {
        assert.notEqual(10, sumar(5, 7));
    });
});


/*
    ssh-keygen -t rsa

    copiar llave pública, ir a gitlab y en Preferences ir a SSH Keys y pegamos la llave

    ir a la carpeta del proyecto
    git remote add origin https://gitlab.com/mus1cian/package_manager_wp_11_1.git (creo que aquí era el otro link que tiene un @ en el nombre)
    git push -u origin master (o si se llama main le pongo main)

    npm i instala las dependencias después de conectarse al repositorio
*/
